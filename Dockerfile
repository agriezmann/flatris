FROM node:alpine

RUN mkdir -p /app

COPY . /app
WORKDIR /app
RUN yarn install && \
    #yarn test && \
    yarn build

EXPOSE 3000

CMD yarn start
